// файл подключения языков
import Vue from 'vue';
import VueI18n from 'vue-i18n';
import lang from './lang.js'; 
import  { store } from '../store/index.js';
Vue.use(VueI18n);
const i18n = new VueI18n({
  locale: store.state.lang, 
  messages: lang.messages
})
export default i18n;