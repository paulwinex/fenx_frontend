// файл подключения роутера и настройки путей

import Vue from 'vue'
import Router from 'vue-router'
import Auth from '../components/themes/default/pages/auth.vue';
import Index from '../components/themes/default/pages/index.vue';
import TeamsList from '../components/themes/default/pages/teams.vue';
import TeamPage from '../components/themes/default/pages/team.vue';
import ProjectsList from '../components/themes/default/pages/projects.vue';
import Settings from '../components/themes/default/pages/settings.vue';
import Page404 from '../components/themes/default/pages/404.vue';
Vue.use(Router)

let router = new Router({ 
	mode: 'history',
	routes: [
    {
      path: '/',
      name: 'auth',
      component: Auth
    },
	{
      path: '/panel',
      name: 'index',
      component: Index
    },
	{
      path: '/teams',
      name: 'teamsList',
      component: TeamsList
    },
   // {
   //    path: '/team/:id',
   //   name: 'teamPage',
   //   component: TeamPage
   // },
	{
      path: '/projects',
      name: 'projectsList',
      component: ProjectsList
    },
	{
      path: '/settings',
      name: 'settings',
      component: Settings
    },
	{
      path: '*',
      name: '404',
      component: Page404
    },
	
  ]
})

export default router;
