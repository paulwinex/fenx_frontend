// модуль для работы с api
import axios from 'axios';
import qs from 'qs';
import {store} from '../store/index.js';
import globals from '../globals/index.js';
var api = axios.create({
	baseURL: globals.api.url,
	headers: { 'Content-Type': 'application/json','apiversion': globals.api.version },
	timeout: globals.api.timeout,
})
api.interceptors.request.use((request) => {
  if (request.data && request.headers['Content-Type'] === 'application/x-www-form-urlencoded') { // преобразование данных в url-строку для нужных запросов
      request.data = qs.stringify(request.data);
  }
  return request;
});

api.request = async function(url,data=null,params=null) { // функция-обертка для POST-вызовов к api, при ошибке показывает уведомление в углу экрана
	let api_url='/api/'+url;
	try {
		let res = await api.post(api_url,data,params);
		if (res.data.success) {
			return res.data.data;
		} else {
			store.commit('notifications/send',{type:'error','text':res.data.error});
			return false;
		}
	} catch(err) {
		throw err;
	}
	
}
function timeout(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
api.mockDataRequest = async function() {
	await timeout(Math.random()*1000);
	return new Date().toUTCString();
}

export default api; 

