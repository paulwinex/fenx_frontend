// Параметры сайта 
const constants = {
		branding:{ // логотип
			logo:false,
			name:'Farm System',
		},
		api:{ // адреса серверов API
			url:'http://api.fenxsystem.ru/',
			version:'0.1',
			timeout:15000,
			ws_url:'ws://api.fenxsystem.ru/client/',
			ws_reconnect_time:10000,
			logs_request_time:5000,
		},
		status_icons: { // иконки статусов
			'com':'check',
			'err':'times',
			'inp':'spinner',
			'que':'arrow-right',
			'sto':'stop',
			'wrn':'exclamation-triangle',			
		}
};
export default constants;