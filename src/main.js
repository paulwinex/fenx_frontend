import Vue from 'vue'
import App from './App'
import router from './router'
import '../static/bootstrap.min.css'
import 'font-awesome/css/font-awesome.css'
import '../static/themify-icons.css'
import '../static/core.css';
import '../static/custom.css'
import axios from 'axios'
import VueAxios from 'vue-axios'
Vue.use(VueAxios, axios);
import BootstrapVue from 'bootstrap-vue';
Vue.use(BootstrapVue);
import 'bootstrap-vue/dist/bootstrap-vue.css'
import { store } from './store/index.js';
import i18n from './i18n/index.js';
Vue.config.productionTip = false

new Vue({
  el: '#app',
  store,
  router,
  i18n,
  template: '<App/>',
  components: { App }
})
