// функции для форматирования даты в нужный формат 
import moment from 'moment';
const functions = {
    show(date) { 
		return moment(date).format('DD.MM.YY HH:mm'); 
	}, 
}

export default functions 