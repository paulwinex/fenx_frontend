// связь Vuex с вебсокетами
import globals from '@/globals/index.js';

function generateUUID () { // Public Domain/MIT
    var d = new Date().getTime();
    if (typeof performance !== 'undefined' && typeof performance.now === 'function'){
        d += performance.now(); //use high-precision timer if available
    }
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
}


export default function newChannelsBridge (bridge) {
    return store => {
        store.subscribe(mutation => {
			if (mutation.type === 'auth/socketConnect') { // юзер залогинен (либо пройдена проверка на наличие токена), вызывается подключение к сокетам 
				console.log('connecting WS');
				bridge.connect(globals.api.ws_url,undefined,{reconnectInterval: globals.api.ws_reconnect_time});
				bridge.listen(function(data) {
					console.log(data);
					if (data.accept!==undefined) { // если пришел ответ о том, что токен принят
						bridge.send({
							auth: store.state.auth.token,
							client_info: {
								client: 'web',
								host: 'test',
								username: store.state.username,
								os: 'test',
								os_version: '0',
								uuid: generateUUID (),
							}
						})
					}
					if (data.auth!==undefined) { // если пришел ответ о статусе авторизации
						if (!data.auth) {				
							store.dispatch('auth/logout');
						} else {
							store.dispatch('getInitData');
						}
					}
					
					if (data.method!==undefined) { 				
							store.dispatch('externalActions/exec',{'method':data.method,'args':data.args,'kwargs':data.kwargs}); 
					}
					if (data.model!==undefined) { // если пришло обновление данных
						let info = {'type':data.action,'pk':data.pk,'data':data.data};					
						switch (data.model) {
							case 'account.team':
								store.dispatch('teams/change',info);
								break;
							case 'projects.project':
								store.dispatch('projects/change',info);
								break;
							case 'projects.job':
								store.dispatch('jobs/change',info);
							case 'projects.task':
								store.dispatch('jobs/changeTask',info);
								break;
							default:
								break;
						}
					}
				})
			}
        })
  }
}