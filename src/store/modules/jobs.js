import api from '../../api/index.js';
export default {
	namespaced: true,
	state: {
		jobsList:[],
		currentJobID:false,
	},
	getters:{
		currentJob: (state, getters) => (id) => { // получение текущей активной джобы
			let job = state.jobsList.filter(job=>job.uuid==state.currentJobID);
			if (job.length>0) {
				return job[0];
			}
			return {tasks:[],status:'err'};
		},
		getJobsCountByProject: (state, getters) => (id) => {
			return state.jobsList.filter(job=>job.project==id).length;
		},
		getJobsByProject: (state, getters) => (id) => {
			return state.jobsList.filter(job=>job.project==id);
		},
	},
	mutations: { 
		lockTask(state,data) { // блокировка таска при нажатии на кнопку
			state.jobsList.forEach((job,i)=>{
				if (job.uuid==data.job_id) {
					job.tasks[data.task_id].locked=data.state;
				}
			})
		},
		lockJob(state,data) { // аналогично с джобой
			state.jobsList.forEach((job,i)=>{
				if (job.uuid==data.uuid) {
					job.locked=data.state;
				}
			})
		},
		create(state,data) { // создание новой джобы
			state.jobsList.push(data.data);
		},
		update(state,data) { // изменение джобы - с проходом по всем данным
			state.jobsList.forEach((job,i)=>{
				if (job.uuid==data.data.uuid) {
					job.locked=false;
					Object.keys(data.data).forEach(key=>{
						state.jobsList[i][key]=data.data[key];
					})
					state.jobsList[i].tasks.forEach(task=>{
						task.locked=false;
					})
				}
			})
		},
		delete(state,uuid) { // удаление джобы по ее id
			state.jobsList.forEach((job,i)=>{
				if (job.uuid==uuid) {
					state.jobsList.splice(i,1);
				}
			})
		},
		setCurrentJob(state,uuid) {
			state.currentJobID=uuid;
		},
		setJobs(state,jobs) {
			state.jobsList=jobs;
		},
		updateTask(state,data) { 
			state.jobsList.forEach((job,i)=>{
				if (job.uuid==data.data.job_uuid) {
					job.tasks.forEach((task,j)=>{
						if (task.id==data.pk) {
							Object.keys(data.data).forEach(key=>{
								state.jobsList[i].tasks[j][key]=data.data[key];							
							})
						}						
					})
				}
			})
		},
		deleteTask(state,data) { 
			state.jobsList.forEach((job,i)=>{
				if (job.uuid==data.job_uuid) {
					job.tasks.forEach((task,j)=>{
						if (task.id==data.pk) {
							state.jobsList[i].tasks.splice(j,1);							
						}						
					})
				}
			})
		},
		createTask(state,data) {
			state.jobsList.forEach((job,i)=>{
				if (job.uuid==data.data.job_uuid) {
					state.jobsList[i].tasks.push(data.data);
				}
			})
		}	
	},
	actions: {
		setJobStatus({ commit },data) { // изменение статуса джобы через api
			commit('lockJob',{uuid:data.job,state:true});
			api.request('set_job_status',{'job_uuid':data.job,'status':data.status}).then(res=>{
				commit('lockJob',{uuid:data.job,state:false});
			});
		},
		setTaskStatus({ commit },data){ // аналогично с таском
			commit('lockTask',{job_id:data.job,task_id:data.task,state:true});
			api.request('set_task_status',{'job_uuid':data.job,'task_index':data.task,'status':data.status}).then(res=>{
				commit('lockTask',{job_id:data.job,task_id:data.task,state:false});
			});
		},
		restartJob({ commit },job) { // перезапуск джобы
			commit('lockJob',{uuid:job,state:true});
			api.request('restart_job',{'job_uuid':job}).then(res=>{
				console.log(res);
				commit('lockJob',{uuid:job,state:false});
			})
		},
		change({ commit },data) { // вызывается при приходе новых данных по ws
			if (data.type=='update') {
				commit('update',{'pk':data.pk,'data':data.data});
			}
			if (data.type=='create') {
				commit('create',{'pk':data.pk,'data':data.data});
			}
			if (data.type=='delete') {
				commit('delete',data.data.uuid);
			}
		},
		changeTask({ commit },data) { 
			if (data.type=='update') {
				commit('updateTask',{'pk':data.pk,'data':data.data});
			}
			if (data.type=='create') {
				commit('createTask',{'pk':data.pk,'data':data.data});
			}
			if (data.type=='delete') {
				commit('deleteTask',{'pk':data.pk,'job_uuid':data.data.job_uuid});
			}
		}
  },
};