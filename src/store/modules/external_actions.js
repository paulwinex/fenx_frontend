// методы для вызова по ws


const newalert=function(args){
	alert('Alert: '+JSON.stringify(args));
}


export default {
	namespaced: true,
	state: {
		
	},
	actions:{
		exec( { commit },data) {
			data=JSON.parse(JSON.stringify(data));
			if (data.method=='show_message') {
				if (data.kwargs.view!='popup') {
					this.commit('notifications/send',{'text':data.kwargs.text,'type':data.kwargs.type,'timeout':data.kwargs.timeout}); 
				} else {
					this.commit('notifications/popup',{'shown':true,'text':data.kwargs.text,'type':data.kwargs.type,'button_text':data.kwargs.button_text});
				}
			}
			
			if (data.method=='disconnect') {
				this.dispatch('auth/logout');
			}
		}
	}
}