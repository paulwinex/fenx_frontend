// модуль для хранения и управления уведомлениями
export default {
	namespaced: true,
	state: {
		notificationsList:[],
		popupData:{
			shown:false,
			type:'',
			text:'',
			button_text:'',
		}
	},
	mutations: { 
		popup(state,data) {
			Object.keys(data).forEach(key=>{
				state.popupData[key]=data[key];
			})
			if (!data.button_text) {
				state.popupData.button_text='OK';
			}
		},
		send(state,data) {
			var id=state.notificationsList.length;
			state.notificationsList.push({'id':id,'text':data.text,'type':data.type,'status':0});
			if (!data.timeout || data.timeout<1) {
				data.timeout=5;
			}
			setTimeout(()=>{
				state.notificationsList.forEach((notification,i)=>{
					if (notification.id==id) {
						state.notificationsList[i].status=-1;
						setTimeout(()=>{
							state.notificationsList.splice(i,1);
						},500);
					}
				})
			},data.timeout*1000);
		},
	},
	actions: {
		
  },
};