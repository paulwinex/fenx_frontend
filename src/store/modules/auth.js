import api from '../../api/index.js';
import router from '../../router/index.js';
export default {
  namespaced: true,
  state: {
	loginResponse:false,
    loginText: '',
    token: localStorage.token,
	loggedIn:!!localStorage.getItem('token'),
	username:'',
	email:'',
  },
  mutations: { 
	setAccountData(state,data) {
		state.username=data.username;
		state.email=data.email;
	},
	setLoggedState(state,value) {
		state.loggedIn=value;
	},
	socketConnect(state) {
		// для использования в Websocket bridge
	},
	loginStart(state) {
		state.loginResponse=true;
		state.loginText='common.loading';
	},
    loginFail(state,errorText) {
		state.loginResponse=true;
		state.loginText='auth.login_fail';
	},
    loginSuccess(state, token) {
		state.loginResponse=true;
		state.token = token;
		localStorage.token=token;
		api.defaults.headers.common['sessionid']=token;
		state.loginText='auth.login_success';
    },
    logout(state) {
	  state.loggedIn=false;
      state.token = '';
	  router.push('/');
    },
  },
  actions: {
	checkToken({ commit }) { // проверка на авторизацию при загрузке страницы
		if (localStorage.token) {
			api.defaults.headers.common['sessionid']=localStorage.token;
			commit('socketConnect');
			commit('setLoggedState',true);
			if (router.currentRoute.path=='/') {
				router.push('/panel');
			}
		} else {
			commit('setLoggedState',false);
			router.push('/');
		}  
	},
	login({ commit },data) { // вход в аккаунт
		commit('loginStart');
		api.post('account/login',{'email':data.email,'password':data.password}).then((res)=> {  
			if (!res.data.accept) {
				 commit('loginFail',res.data.error);
			} else {
				commit('loginSuccess',res.data.keys.sessionid);
				setTimeout(()=>{ commit('socketConnect'); router.push('/panel')},3500)
			}
		})
    },
    logout({ commit }) { // выход из аккаунта
      localStorage.token = '';
      commit('logout');
    },
  },
};