import api from '../../api/index.js';
import router from '../../router/index.js';
export default {
	namespaced: true,
	state: {
		projectsList:{},
	},
	getters:{
		getProjectNameByID: (state,getters) => (id)=>{
			let proj = state.projectsList.filter(project=>project.uuid==id);
			if (proj.length>0) {
				return proj[0].name;
			}
			return '';
		},
		getProjectsCountByTeam: (state, getters) => (id) => {
			return state.projectsList.filter(project=>project.team==id).length;
		},
		getProjectsByTeam: (state, getters) => (id) => {
			return state.projectsList.filter(project=>project.team==id);
		},
	},
	mutations: { 
		create(state,data) {
			state.projectsList.push(data.data);
		},
		update(state,data) {
			state.projectsList.forEach((project,i)=>{
				if (project.uuid==data.data.uuid) {
					Object.keys(data.data).forEach(key=>{
						state.projectsList[i][key]=data.data[key];
					})
				}
			})
		},
		delete(state,uuid) {
			state.projectsList.forEach((project,i)=>{
				if (project.uuid==uuid) {
					state.projectsList.splice(i,1);
				}
			})
		},
		setMyProjects(state,data) {
			state.projectsList=data;
		},
	},
	actions: {
		change({ commit },data) {
			if (data.type=='update') {
				commit('update',{'pk':data.pk,'data':data.data});
			}
			if (data.type=='create') {
				commit('create',{'pk':data.pk,'data':data.data});
			}
			if (data.type=='delete') {
				commit('delete',data.data.uuid);
			}
		}
  },
};