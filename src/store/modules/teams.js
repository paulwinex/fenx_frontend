import api from '../../api/index.js';
import router from '../../router/index.js';
export default {
	namespaced: true,
	state: {
		teamsList:[],
		currentTeam:-1,
		currentPanelTeam:-1,
	},
	getters:{
		currentPanelTeamData: (state, getters) => {
			let team = state.teamsList.filter(team=>team.id==state.currentPanelTeam);
			if (team.length>0) {
				return team[0];
			}
			return {};
		},
		currentTeamBalance: (state,getters)=>{
			let score=0;
			state.teamsList.forEach(team=>{
				if (team.id==state.currentTeam) {
					score=team.current_score;
				}
			})
			return '$'+score;
		},
		getTeamByID: (state, getters) => (id) => {
			let currentTeam = false;
			state.teamsList.forEach(team=>{
				if (team.id==id) {
					currentTeam=team;
				}
			})
			return currentTeam;
		},
		currentTeamData: (state, getters) => {
			let currentTeam = false;
			state.teamsList.forEach(team=>{
				if (team.id==state.currentTeam) {
					currentTeam=team;
				}
			})
			return currentTeam;
		},
	},
	mutations: { 
		update(state,data) {
			state.teamsList.forEach((team,i)=>{
				if (team.id==data.pk) {
					Object.keys(data.data).forEach(key=>{
						state.teamsList[i][key]=data.data[key];
					})
				}
			})
		},
		create(state,data) {
			state.teamsList.push(data.data);
		},
		delete(state,uuid) {
			state.teamsList.forEach((team,i)=>{
				if (team.id==uuid) {
					state.teamsList.splice(i,1);
				}
			})
		},
		setCurrentPanelTeam(state,id) {
			state.currentPanelTeam=id;
		},
		setCurrentTeam(state,id) {
			state.currentTeam=id;
			localStorage.currentTeam=id;
		},
		setMyTeams(state,data) {
			state.teamsList=data;
		},
	},
	actions: {
		change({ commit },data) {
			if (data.type=='update') {
				commit('update',{'pk':data.pk,'data':data.data});
			}
			if (data.type=='create') {
				commit('create',{'pk':data.pk,'data':data.data});
			}
			if (data.type=='delete') {
				commit('delete',data.data.uuid);
			}
		}
  },
};