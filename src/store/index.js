import Vue from 'vue';
import Vuex from 'vuex';
import api from '../api/index.js';

import auth from './modules/auth.js';
import teams from './modules/teams.js';
import projects from './modules/projects.js';
import jobs from './modules/jobs.js';
import notifications from './modules/notifications.js';
import externalActions from './modules/external_actions.js';

import newChannelsBridge from './plugins/bridge.js';
import { WebSocketBridge } from 'django-channels';

Vue.use(Vuex);

const bridge= new WebSocketBridge();
const bridgePlugin = newChannelsBridge(bridge);
export const store = new Vuex.Store({
  modules: {
	  auth,teams,projects,jobs,notifications,externalActions
  },
  plugins:[
	  bridgePlugin
  ],
  state: {
		lang:localStorage.lang ? localStorage.lang : 'en',
		initDataLoaded:false,  
		jobPanel:{
			opened:false,
			lastTimeOpened:0,
			tab:0,
		},
		teamPanel:{
			opened:false,
			lastTimeOpened:0,
			tab:0,
		},
		lightbox:{
			shown:false,
			picture:'',
			lastTimeClosed:0,
		}
},
  mutations:{
		setLang(state,lang) { // смена языка
			state.lang=lang;
			localStorage.lang=lang;
			location.reload();
		},
		initDataWasLoaded(state){
			state.initDataLoaded=true;
		},
		setJobPanel(state,data) { // управление панелью джобов
			state.jobPanel.opened=data.state;
			if (data.state){
				state.jobPanel.lastTimeOpened=new Date().getTime();
			}
			if (data.tab!==undefined) {
				state.jobPanel.tab=data.tab;
			} else {
				state.jobPanel.tab=1;
			}
		},
		setTeamPanel(state,data) {
			state.teamPanel.opened=data.state;
			if (data.state){
				state.teamPanel.lastTimeOpened=new Date().getTime();
			}
			if (data.tab!==undefined) {
				state.teamPanel.tab=data.tab;
			} else {
				state.teamPanel.tab=1;
			}
		},
		hideLightbox(state) { // управление лайтбоксом
			state.lightbox.shown=false;
			state.lightbox.lastTimeClosed=new Date().getTime();
		},
		showLightbox(state,picture) {
			state.lightbox.shown=true;
			state.lightbox.picture=picture;
		}
  },
  actions:{
	getInitData({dispatch, commit }) { // получение начальных данных, их распределение по модулям
		api.request('init_data').then((data)=>{
			let account = data.account;
			let teams = data.teams;
			let projects=[];
			let jobs=[];
			teams.forEach(team=>{
				team.projects.forEach(project=>{
					project.team=team.id;
					project.jobs.forEach(job=>{
						job.project=project.uuid;
						jobs.push(job);
					})
					delete project.jobs;
					projects.push(project);
				});
				delete team.projects;
			})
			commit('auth/setAccountData',account);
			commit('teams/setMyTeams',teams);
			if (teams.length>0) {
				if (localStorage.currentTeam===undefined) {
					localStorage.currentTeam=teams[0].id;
				}
				commit('teams/setCurrentTeam',localStorage.currentTeam);
			}
			commit('projects/setMyProjects',projects);
			commit('jobs/setJobs',jobs);
			commit('initDataWasLoaded');
		})
	},
  }
})
