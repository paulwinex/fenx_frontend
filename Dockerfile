FROM ubuntu:16.04
RUN apt-get update &&\
    apt-get install -yqq mc curl git net-tools wget &&\
    curl -sL https://deb.nodesource.com/setup_8.x | bash - &&\
    apt-get install -y nodejs &&\
    mkdir -p /data
COPY . /data/fenx_web/
WORKDIR /data/fenx_web
RUN cd /data/fenx_web && npm install
EXPOSE 8080
ENTRYPOINT npm run dev
